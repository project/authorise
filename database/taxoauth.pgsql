
---permission - term authorisation relationship
DROP TABLE taxoauth_permission_term;
CREATE TABLE taxoauth_permission_term (
  perm varchar NOT NULL default '',
  tid integer NOT NULL default 0
);

---node - term authorisation relationship
DROP TABLE taxoauth_node_term;
DROP TABLE taxoauth_nodetype_term;
CREATE TABLE taxoauth_nodetype_term (
  nid varchar NOT NULL default '',
  tid integer NOT NULL default 0
);